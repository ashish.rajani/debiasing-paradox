# Debiasing Paradox

Selecting the most desired person from a large pool of candidates is one of the most common yet crucial decision-making
processes that we encounter in our society today. Selection based on a defined quota to preserve a proportionate
representation of protected group members is one of the most commonly used methods.

As the research suggests applying quota debiasing methods can have unintended negative consequences. In the research
paper on Debiasing Paradox, Smirnov et. al. demonstrates that using a single correlate attribute for quota-based
debiasing may result in reduced representation for an already underrepresented group and increased selection bias.

In this study, we explore the effect when we apply different debiasing algorithms at different (pre-, in- and post-)
processing stages. Our results demonstrate that debiasing can in turn lead to bias against the already most
underrepresented population. Hence, while applying such methods one must be cautious of such shortfalls and more effort
should need to be put into eliminating the underlying cause of the bias.

This repository contains all the code that was used to perform the experiments as well as to prepare the report. 
Furthermore, the code was forked from the git repository 'debiasing' by the authors of the above mentioned paper. 

## Requirements
    - Python 3.7 or above
    - Jupyter Notebook
    - Pandas
    - blackboxauditing 0.1.54
    - fairsearchcore 1.0.4
    - fairsearchdeltr 1.0.2
    - matplotlib 3.5.0
    - numpy 1.21.2
    - pandas 1.3.4

## Instructions
The code is bifurcated based on the dataset used. Each dataset folder has 5 jupyter Notebook file i.e. plots (to see saved results), main (to run quota based debiasing, preprocessing and preliminary analysis), pre (implementation for pre-processing debiasing algorithm), in (implementation for in-processing debiasing algorithm) and post (implementation for post-processing debiasing algorithm)
